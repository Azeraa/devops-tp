# Dockerfile
FROM php:8.2-fpm

RUN apt-get update \
    && apt-get install -y \
        libpq-dev \
    && docker-php-ext-install pdo_pgsql

COPY ./php.ini /usr/local/etc/php/conf.d/custom.ini

WORKDIR /var/www

EXPOSE 9000
CMD ["php-fpm"]
