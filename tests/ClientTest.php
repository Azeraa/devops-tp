<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Client;
use App\Entity\Location;

class ClientTest extends TestCase
{
    public function testCreateClient(): void
    {
        $client = new Client();
        $client->setCin('123456');
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdress('123 Main St');
        $this->assertInstanceOf(Client::class, $client);
        $this->assertEquals('123456', $client->getCin());
    }

    public function testUpdateClient(): void
    {
        $client = new Client();
        $client->setCin('123456');
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdress('123 Main St');
        $client->setNom('UpdatedName');
        $client->setAdress('456 New St');
        $this->assertEquals('UpdatedName', $client->getNom());
    }

    public function testDeleteClient(): void
    {
        $client = new Client();
        $client->setCin('123456');
        $client->setNom('John');
        $client->setPrenom('Doe');
        $client->setAdress('123 Main St');
        $this->assertNull($client->getId()); // Assuming the ID is set to null after deletion
    }
}
