<?php

namespace App\Tests;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class VoitureTest extends TestCase

{
    public function testCreateVoiture(): void
    {
        $voiture = new Voiture();
        $voiture->setSerie('ABC123');
        $voiture->setDateMiseEnMarche(new \DateTime('2022-01-31'));
        $voiture->setModele("11");
        $voiture->setPrixJour(100.50);
        $this->assertInstanceOf(Voiture::class, $voiture);
        $this->assertEquals('ABC123', $voiture->getSerie());
    }

    public function testUpdateVoiture(): void
    {
        $voiture = new Voiture();
        $voiture->setSerie('ABC123');
        $voiture->setDateMiseEnMarche(new \DateTime('2022-01-31'));
        $voiture->setModele("11");
        $voiture->setPrixJour(100.50);
        $voiture->setModele("12");
        $voiture->setPrixJour(120.75);
        $this->assertEquals('ABC123', $voiture->getSerie());
    }

    public function testDeleteVoiture(): void
    {
        $voiture = new Voiture();
        $voiture->setSerie('ABC123');
        $voiture->setDateMiseEnMarche(new \DateTime('2022-01-31'));
        $voiture->setModele("11");
        $voiture->setPrixJour(100.50);
        $this->assertNull($voiture->getId());
    }

}
