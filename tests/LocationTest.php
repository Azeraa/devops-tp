<?php

namespace App\Tests;
use App\Entity\Location;
use App\Entity\Client;
use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;

class LocationTest extends TestCase
{
    public function testCreateLocation(): void
    {
        $location = new Location();
        $location->setDateDebut(new \DateTime('2022-02-01'));
        $location->setDateRetour(new \DateTime('2022-02-10'));
        $location->setPrix(150.75);
        $client = new Client();
        $voiture = new Voiture();
        $location->setClient($client);
        $location->setVoiture($voiture);
        $this->assertInstanceOf(Location::class, $location);
        $this->assertEquals(new \DateTime('2022-02-01'), $location->getDateDebut());
    }

    public function testUpdateLocation(): void
    {
        $location = new Location();
        $location->setDateDebut(new \DateTime('2022-02-01'));
        $location->setDateRetour(new \DateTime('2022-02-10'));
        $location->setPrix(150.75);
        $client = new Client();
        $voiture = new Voiture();
        $location->setClient($client);
        $location->setVoiture($voiture);
        $location->setPrix(180.50);
        $this->assertEquals(180.50, $location->getPrix());
    }

    public function testDeleteLocation(): void
    {
        $location = new Location();
        $location->setDateDebut(new \DateTime('2022-02-01'));
        $location->setDateRetour(new \DateTime('2022-02-10'));
        $location->setPrix(150.75);
        $client = new Client();
        $voiture = new Voiture();
        $location->setClient($client);
        $location->setVoiture($voiture);
        $this->assertNull($location->getId());
    }
}
